﻿<!--Check Session user-->
<?php 
	session_start(); 
	if (!isset($_SESSION['username'])) 
	{
		$_SESSION['msg'] = "You must log in first";
		header('location: login.php');
	}
	if (isset($_GET['logout'])) 
	{
		session_destroy();
		$mysqli->close();
		unset($_SESSION['username']);
		header("location: login.php");
	}
?>

<!--DB Connect-->
<?php include "connect.php"?>
<!--HTML-->
<html style="font-family: 'Karla', sans-serif;">
	<head>
		<link href="https://fonts.googleapis.com/css?family=Karla" rel="stylesheet">
		<link rel="stylesheet" type="text/css" href="http://fonts.googleapis.com/css?family=Tangerine">
  		<link rel="stylesheet" type="text/css" href="css/style.css" >
  		<link rel="stylesheet" type="text/css" href="css/event.css" >
  		<link rel="stylesheet" type="text/css" href="css/addevent.css" >
  		<link rel="stylesheet" type="text/css" href="css/stylemenu.css" >
		<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
		<script src="js/event.js"></script>
		<script src="js/calendarjs.js"></script>
	  </head>

	  	<!--Menu button-->
  		<body class="calendar" style="background-color:#FFFAF0 ; position:relative; ">
 				<span class="box" style="left:-144px; top:220px;">
					<button class="show button" style="background-color:#33CCCC;">Menu</button>
						<div class="men_ex">
							<ul>
								<li><a href="index.php?">Home</a></li>
								<li><a href="index.php?logout='1'" style="color:red">logout</a></li>
							</ul>
						</div>
				</span>
				  
				<!--Get user-->
				<?php 
					$qry = "SELECT * FROM users";
					$result = $mysqli->query($qry);

					while ($row = $result->fetch_array())
					{
						$user = $row['username'];
						
						if(isset($_SESSION['username']))
						{
	 						if ($user == $_SESSION['username'])
							{
							 	$user2 = $_SESSION['username'];
	 						}
						}
					}
			
				?>
					<!--Add event button-->
					<span class="box" style="left:-333px; top:140px; font-size: 48px;   text-align: center; border: 1px solid ; background-color:#ccffcc; width:300px; height:70px; border:none; "><?php echo  $_SESSION['username']; ?></span>
					<span class="box" style="left:-333px; top:220px;">
						<button id="myBtn" class="button"  >AddEvent  </button>
							<div id="myModal" class="modal">
  								<div class="modal-content">
									<span class="close">&times;</span> 
										<p>Add Event...!</p>
   											<form action="addapp.php" method="post">
            										Date: <input type="date" name="date" onblur="return checkempty()" id="date">
													<span id="star_fore" class="star">*</span>
                									<div id="error_fore" class="redcolor"></div>
													Title : <input type="text" name="title" /><br />
            										Details : <input type="text" name="details" /><br />
            										Start time : <input type="time" name="timeStart" /><br />
            										End time : <input type="time" name="timeEnd" /><br />
            									
            										<input type="submit" value="Add" id="sub" style="display: none;"> </span>
        									</form>
  								</div>
						</div>
					</span>
				<!--Show month year and insert update delete selecte-->	
				<?php
						include "updatedb.php";
						echo '<div class="monthname" ">' . $nameM . '<br></div>' ;
						echo '<div  class="monthname" style="font-size:48px;">' . $year . '<br></div>' ;
				?>

					<!--Previous button-->
					<a style="text-decoration: none;  " href="?date=<?php echo $prevM; ?>"><div class="button" style="position:absolute; left:47px; padding: 1px 20px; background-color:#FF99CC; box-shadow:none;" ><=~</div></a>
					<!--Next button-->
					<a style="text-decoration: none; " href="?date=<?php echo $nextM; ?>"><span class="button "style="position:absolute; right:40px; padding: 1px 20px; background-color:#FF99CC;box-shadow: none;" >~=></span><br><br></a>
					<!--Day button-->
					<a style="text-decoration: none; " href="dayview.php?date=<?php echo $year."-".$month."-".$day; ?>"><span class="button "style="position:absolute;right: 495px;top: 143px;padding: 1px 20px;background-image: linear-gradient(120deg, #d4fc79 0%, #96e6a1 100%);box-shadow: none; color:black;" >Day</span></a>
					<!--Month button-->
					<a style="text-decoration: none; " href="weekview.php?date=<?php echo $year."-".$month."-".$day; ?>"><span class="button "style="position:absolute;right: 390px;top: 143px;padding: 1px 20px;background-image: linear-gradient(120deg, #d4fc79 0%, #96e6a1 100%);box-shadow: none; color:black;" >Week</span></a>
						
						<!--Show calendar-->
						<div class="calendar " > 
   							<div class="days" style="background:#FF9797">Sunday</div> 
   							<div class="days" style="background:#FFFF88">Monday</div> 
   							<div class="days" style="background:#FDB4BF">Tuesday</div> 
   							<div class="days" style="background:#88DDBB">Wednesday</div> 
   							<div class="days" style="background:#FFBE7D">Thursday</div> 
   							<div class="days" style="background:#A4C8F0">Friday</div> 
   							<div class="days" style="background:#D0B3E1">Saturday</div> 
							   
							   <!--Show blankday-->
							   <?php  
   									for($i=1; $i<=$firstday; $i++) 
									{ 
											echo '<div style="background:#eeecec" class="date "></div>'; 
									} 
								?> 
								
								<!--Show day-->
								<?php 
		   							for($i=1; $i<=$days; $i++) 
									{ 
										echo '<div id="'. $i . '" ondblclick="goday('. $i . ')" ondrop="drop(event)" ondragover="allowDrop(event)" class="date '; 
										if ($today == $i && $todaymonth==$month && $todayyear == $year) 
										{ 
											echo '" style="background:#cfc;"'; 
										}
										else
										{
											echo '" style="background:white"'; 
										}
											echo '"><div class="color" >' . $i . '<br></div>'; 

											$qry = "SELECT * FROM appointments ORDER BY time1";
											$result = $mysqli->query($qry);

											while ($row = $result->fetch_array())
											{
												$dbday = date('d',strtotime($row['date']));
												$dbmonth = date('m',strtotime($row['date']));
												$dbyear = date('Y',strtotime($row['date']));
												$dbtitle = $row['title'];
												$dbdetial = $row['details'];
												$dbuser = $row['username'];
												$dbid = $row['id'];
												
												//Show title
												if($dbday==$i&&$dbmonth==$month&&$dbyear==$year && $dbuser ==$user2)
												{
													echo '<div id="'."flip".$dbid.'" draggable="true" ondragstart="drag(event)" style="background:#FFBEAC; ">'. $dbtitle   ;
													echo  '<div style="border-radius:3%;background:#ffffcc;" id="'."panel".$dbid.'">' .  $dbdetial. '<br><button onclick="edit('.$dbid.')">Edit</button><button id="'.$dbid.'"><a style=" text-decoration: none; color:red;" href="javascript:AlertIt('.$dbid.')">Delete</a></button>'.'</div></div>' ;
												}
											}
											echo '</div>';
									} 
								?>

								<!--Show blankday-->
								<?php 
										$daysleft = 7-(($days + $firstday)%7); 
										if($daysleft<7) 
										{ 
											for($i=1; $i<=$daysleft; $i++) 
											{ 
												echo '<div style="background:#eeecec"class="date "></div>'; 
											} 
										} 
								?> 
						</div>
						
	<script>
	//Drag and Drop
	function allowDrop(ev) 
	{
		ev.preventDefault();
	}

	function drag(ev) 
	{
		ev.dataTransfer.setData("text", ev.target.id);
	}

	function drop(ev) 
	{
		ev.preventDefault();
		var data = ev.dataTransfer.getData("text");
		ev.target.appendChild(document.getElementById(data));
		var str = ev.target.lastChild.id;
		var str2= str.substr(4);
		$.post( "update.php", { year: "<?php echo $year; ?>", month: "<?php echo $month; ?>", day: ev.target.id, id: str2  } );
	}
	//Add event block
	var modal = document.getElementById('myModal');
	var btn = document.getElementById("myBtn");
	var span = document.getElementsByClassName("close")[0];
	btn.onclick = function() 
	{
		modal.style.display = "block";
	}

	span.onclick = function() 
	{
		modal.style.display = "none";
	}

	//Comfirm delete
	function AlertIt($id) 
	{
	
		var answer = confirm ("Comfirm Delete...")
		if (answer){
		$.post( "update.php", { idbotdel : $id } );
		window.location="./calendar.php?date=<?php echo $_GET['date'];?>";}
	}
	
	//Go day
	function goday($i) 
	{
		<?php 
		$monthgoday = date('m', strtotime($_GET['date']));      
		$yeargoday = date('Y', strtotime($_GET['date']));
		?>
		window.location="./dayview.php?date=<?php echo $yeargoday."-".$monthgoday."-";?>"+$i;
	}
	//Edit title
	function edit($i) 
	{
		window.location="./edit.php?date=<?php echo $_GET['date'];?>&id="+$i;
	}
	</script>
</body>
</html>