-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:3306
-- Generation Time: Dec 13, 2018 at 01:30 PM
-- Server version: 5.7.23
-- PHP Version: 7.2.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `calendar`
--

-- --------------------------------------------------------

--
-- Table structure for table `appointments`
--

DROP TABLE IF EXISTS `appointments`;
CREATE TABLE IF NOT EXISTS `appointments` (
  `id` int(30) NOT NULL AUTO_INCREMENT,
  `date` date NOT NULL,
  `title` text NOT NULL,
  `details` text NOT NULL,
  `username` text NOT NULL,
  `time1` time NOT NULL,
  `time2` time NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `appointments`
--

INSERT INTO `appointments` (`id`, `date`, `title`, `details`, `username`, `time1`, `time2`) VALUES
(1, '2018-12-09', 'Teaching C++', 'ISNE#5 always ask about C++ ', '_kenneth', '01:00:00', '04:00:00'),
(2, '2018-12-02', ' Vacation Time', 'Watching a student doing Calendar for a hour.', '_kenneth', '09:00:00', '13:00:00'),
(3, '2018-12-04', '  My wife', 'My wife need sport car and new house . Buy it today!', '_kenneth', '03:00:00', '12:00:00'),
(4, '2018-12-17', ' Grade', 'Give grade A+++++ to 600611001', '_kenneth', '03:00:00', '13:00:00'),
(5, '2018-12-03', '  Teach Webpro', 'Web programming language', '_kenneth', '04:00:00', '07:00:00'),
(6, '2018-12-10', 'SendCalendar', 'Enjoy Students Calendar', '_kenneth', '10:00:00', '13:00:00'),
(7, '2018-12-11', 'Say Wow!', 'Wowwwwwwwwwwwww!', '_kenneth', '11:00:00', '14:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
CREATE TABLE IF NOT EXISTS `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(100) NOT NULL,
  `email` varchar(100) NOT NULL,
  `password` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `username`, `email`, `password`) VALUES
(15, '_kenneth', 'samekrub@hotmail.com', '227b26edad65be0751b64d5b2f2c90e1');
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
