<!--Check Session-->
<?php 
	session_start(); 
	if (!isset($_SESSION['username'])) 
	{
		$_SESSION['msg'] = "You must log in first";
		header('location: login.php');
	}

  if (isset($_GET['logout']))
  {
		session_destroy();
		unset($_SESSION['username']);
		header("location: login.php");
	}
?>
<!--Connect DB-->
<?php include "connect.php"?>
<!--HTML-->
<html style="font-family: 'Karla', sans-serif;">
  <head>
    <link href="https://fonts.googleapis.com/css?family=Karla" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="css/stylemenu.css" >
    <link rel="stylesheet" type="text/css" href="css/dayview.css" >
    <link rel="stylesheet" type="text/css" href="css/weekview.css" >
    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
  </head>
    <body  style="margin:0 auto; width:1000px; background:#FFE3BE">
      <div  class="bg">
          <?php
                $user2=$_SESSION['username'];
                $day = date('d', strtotime($_GET['date']));      
                $month = date('m', strtotime($_GET['date']));  
                $year = date('Y', strtotime($_GET['date'])); 
                $nameM = date('F', mktime(0,0,0, $month,10));
                $newdate  = strtotime($_GET['date']);
                $nextW = date('Y-m-d',strtotime('+1 week',$newdate));
                $prevW = date('Y-m-d',strtotime('-1 week',$newdate));
                
                $date1 = date('Y-m-d',strtotime('+0 day',$newdate)); 
                $day11 = date('d', strtotime($date1));   
                $month11 = date('m', strtotime($date1));  
                $year11 = date('Y', strtotime($date1)); 

                $date2= date('Y-m-d',strtotime('+1 day',$newdate)); 
                $day22 = date('d', strtotime($date2));   
                $month22 = date('m', strtotime($date2));  
                $year22 = date('Y', strtotime($date2)); 

                $date3 = date('Y-m-d',strtotime('+2 day',$newdate)); 
                $day33 = date('d', strtotime($date3));   
                $month33 = date('m', strtotime($date3));  
                $year33 = date('Y', strtotime($date3)); 

                $date4 = date('Y-m-d',strtotime('+3 day',$newdate)); 
                $day44 = date('d', strtotime($date4));   
                $month44 = date('m', strtotime($date4));  
                $year44 = date('Y', strtotime($date4)); 


                $date5 = date('Y-m-d',strtotime('+4 day',$newdate)); 
                $day55 = date('d', strtotime($date5)); 
                $month55 = date('m', strtotime($date5));  
                $year55 = date('Y', strtotime($date5)); 

                $date6 = date('Y-m-d',strtotime('+5 day',$newdate)); 
                $day66 = date('d', strtotime($date6)); 
                $month66 = date('m', strtotime($date6));  
                $year66 = date('Y', strtotime($date6)); 

                $date7 = date('Y-m-d',strtotime('+6 day',$newdate));
                $day77 = date('d', strtotime($date7)); 
                $month77 = date('m', strtotime($date7));  
                $year77 = date('Y', strtotime($date7)); 
          ?>
            <!--Previous Week-->
            <a style="text-decoration: none; " href="?date=<?php echo $prevW; ?>"><div class="button" style="position:absolute;left: 311px;padding: 1px 10px;top: -98px;background-color:#FF99CC;box-shadow:none;text-decoration: none;color:white;border-radius:10%;font-size: 19px;" >-1 week</div></a>
            <!--Next Week-->
            <a style="text-decoration: none; " href="?date=<?php echo $nextW; ?>"><div class="button" style="position:absolute;left: 623px;padding: 1px 10px;top: -98px;background-color:#FF99CC;box-shadow:none;text-decoration: none;color:white;border-radius:10%;font-size: 19px;">+1 week</div></a>
            <!--Show username-->
            <span style="left: 381px;top: -104px;font-size: 42px;text-align: center;border: 1px solid;background-color:#ccffcc;width: 234px;height: 55px;border: 1px dashed;position: absolute;"><?php echo  $user2; ?></span>
            <!--Month-->
            <a style="text-decoration: none; " href="calendar.php?date=<?php echo $_GET['date']; ?>"><span class="button" style="position:absolute;left: 498px;top: -141px;padding: 1px 15px;background-image: linear-gradient(120deg, #d4fc79 0%, #96e6a1 100%);box-shadow: none; color:black ; text-align: center;" >Month</span></a>
            <!--Day-->
            <a style="text-decoration: none; " href="dayview.php?date=<?php echo $_GET['date']; ?>"><span class="button" style="position:absolute;left: 400px;top: -141px;padding: 1px 15px;background-image: linear-gradient(120deg, #d4fc79 0%, #96e6a1 100%);box-shadow: none; color:black ; text-align: center;" >Day</span></a>
         
                <?php
                    echo '<div class="day" style="left: -200px;  top: -33px;" >' .$day11.'</div>';
                    echo '<div class="day" style="left: 20px;  top: -33px;" >' .$day22.'</div>';
                    echo '<div class="day" style="left: 233px;  top: -33px;" >' .$day33.'</div>';
                    echo '<div class="day" style="left: 443px;  top: -33px;" >' .$day44.'</div>';
                    echo '<div class="day" style="left: 654px;  top: -33px;" >' .$day55.'</div>';
                    echo '<div class="day" style="left: 868px;  top: -33px;" >' .$day66.'</div>';
                    echo '<div class="day" style="left: 1083px;  top: -33px;" >' .$day77.'</div>';
                ?>
    <!--Week-->
    <!--Day 1-->
    <div style="top:135px;   left: -245px;position: absolute;">
    <div id ="<?php echo $year11."-".$month11."-".$day11;?>" class="box_date" ondrop="drop(event)" ondragover="allowDrop(event)" ><?php echo $day11."-".$month11."-".$year11;?>
       <?php
                                            $qry = "SELECT * FROM appointments ORDER BY time1";
											$result = $mysqli->query($qry);

											while ($row = $result->fetch_array())
											{
												$dbday = date('d',strtotime($row['date']));
												$dbmonth = date('m',strtotime($row['date']));
												$dbyear = date('Y',strtotime($row['date']));
												$dbtitle = $row['title'];
												$dbuser =$row['username'];
                                                $dbstart = $row['time1'];
                                                $dbend = $row['time2'];
                                                $dbid = $row['id'];
                                                $dbstart_new = substr($dbstart,0,5);
                                                $dbend_new = substr($dbend,0,5);
												if($dbday==$day11&&$dbmonth==$month11&&$dbyear==$year11 && $dbuser ==$user2)
												{
													echo '<div draggable="true" ondragstart="drag(event)" id="'.$dbid.'" class="title_day">' .$dbstart_new."-".$dbend_new." ".$dbtitle. '</div>';
												}
											}
        ?>
    </div>
    <!--Day 2-->
    <div id ="<?php echo $year22."-".$month22."-".$day22;?>" class="box_date" style="left:213px;" ondrop="drop(event)" ondragover="allowDrop(event)" ><?php echo $day22."-".$month22."-".$year22;?>
        <?php
                                            $qry = "SELECT * FROM appointments ORDER BY time1";
											$result = $mysqli->query($qry);

											while ($row = $result->fetch_array())
											{
												$dbday = date('d',strtotime($row['date']));
												$dbmonth = date('m',strtotime($row['date']));
												$dbyear = date('Y',strtotime($row['date']));
												$dbtitle = $row['title'];
												$dbuser =$row['username'];
                                                $dbstart = $row['time1'];
                                                $dbend = $row['time2'];
                                                $dbstart_new = substr($dbstart,0,5);
                                                $dbend_new = substr($dbend,0,5);
                                                $dbid = $row['id'];
												if($dbday==$day22&&$dbmonth==$month22&&$dbyear==$year22 && $dbuser ==$user2)
												{
													echo '<div draggable="true" ondragstart="drag(event)" id="'.$dbid.'"  class="title_day">' .$dbstart_new."-".$dbend_new." ".$dbtitle. '</div>';
												}
											}
        ?>
    </div>
    <!--Day 3-->
    <div id ="<?php echo $year33."-".$month33."-".$day33;?>" class="box_date" style="left:426px;" ondrop="drop(event)" ondragover="allowDrop(event)" ><?php echo $day33."-".$month33."-".$year33;?>
        <?php
                                            $qry = "SELECT * FROM appointments ORDER BY time1";
											$result = $mysqli->query($qry);

											while ($row = $result->fetch_array())
											{
												$dbday = date('d',strtotime($row['date']));
												$dbmonth = date('m',strtotime($row['date']));
												$dbyear = date('Y',strtotime($row['date']));
												$dbtitle = $row['title'];
												$dbuser =$row['username'];
                                                $dbstart = $row['time1'];
                                                $dbend = $row['time2'];
                                                $dbid = $row['id'];
                                                $dbstart_new = substr($dbstart,0,5);
                                                $dbend_new = substr($dbend,0,5);
												if($dbday==$day33&&$dbmonth==$month33&&$dbyear==$year33 && $dbuser ==$user2)
												{
													echo '<div draggable="true" ondragstart="drag(event)" id="'.$dbid.'"  class="title_day">' .$dbstart_new."-".$dbend_new." ".$dbtitle. '</div>';
												}
											}
        ?>
    </div>
    <!--Day 4-->
    <div id ="<?php echo $year44."-".$month44."-".$day44;?>" class="box_date" style="left:639px;" ondrop="drop(event)" ondragover="allowDrop(event)"><?php echo $day44."-".$month44."-".$year44;?>
           <?php
                                            $qry = "SELECT * FROM appointments ORDER BY time1";
											$result = $mysqli->query($qry);

											while ($row = $result->fetch_array())
											{
												$dbday = date('d',strtotime($row['date']));
												$dbmonth = date('m',strtotime($row['date']));
												$dbyear = date('Y',strtotime($row['date']));
												$dbtitle = $row['title'];
												$dbuser =$row['username'];
                                                $dbstart = $row['time1'];
                                                $dbend = $row['time2'];
                                                $dbid = $row['id'];
                                                $dbstart_new = substr($dbstart,0,5);
                                                $dbend_new = substr($dbend,0,5);
												if($dbday==$day44&&$dbmonth==$month44&&$dbyear==$year44 && $dbuser ==$user2)
												{
													echo '<div draggable="true" ondragstart="drag(event)" id="'.$dbid.'" class="title_day">' .$dbstart_new."-".$dbend_new." ".$dbtitle. '</div>';
												}
											}
        
        ?>
    </div>
    <!--Day 5-->
    <div id ="<?php echo $year55."-".$month55."-".$day55;?>" class="box_date" style="left:852px;" ondrop="drop(event)" ondragover="allowDrop(event)"><?php echo $day55."-".$month55."-".$year55;?>
        <?php
                                            $qry = "SELECT * FROM appointments ORDER BY time1";
											$result = $mysqli->query($qry);

											while ($row = $result->fetch_array())
											{
												$dbday = date('d',strtotime($row['date']));
												$dbmonth = date('m',strtotime($row['date']));
												$dbyear = date('Y',strtotime($row['date']));
												$dbtitle = $row['title'];
												$dbuser =$row['username'];
                                                $dbstart = $row['time1'];
                                                $dbid = $row['id'];
                                                $dbend = $row['time2'];
                                                $dbstart_new = substr($dbstart,0,5);
                                                $dbend_new = substr($dbend,0,5);
												if($dbday==$day55&&$dbmonth==$month55&&$dbyear==$year55 && $dbuser ==$user2)
												{
													echo '<div draggable="true" ondragstart="drag(event)" id="'.$dbid.'" class="title_day">' .$dbstart_new."-".$dbend_new." ".$dbtitle. '</div>';
												}
											}
        ?>
    </div>
    <!--Day 6-->
    <div id ="<?php echo $year66."-".$month66."-".$day66;?>" class="box_date" style="left:1065px;" ondrop="drop(event)" ondragover="allowDrop(event)"><?php echo $day66."-".$month66."-".$year66;?>
        <?php
                                            $qry = "SELECT * FROM appointments ORDER BY time1";
											$result = $mysqli->query($qry);

											while ($row = $result->fetch_array())
											{
												$dbday = date('d',strtotime($row['date']));
												$dbmonth = date('m',strtotime($row['date']));
												$dbyear = date('Y',strtotime($row['date']));
												$dbtitle = $row['title'];
												$dbuser =$row['username'];
                                                $dbstart = $row['time1'];
                                                $dbend = $row['time2'];
                                                $dbid = $row['id'];
                                                $dbstart_new = substr($dbstart,0,5);
                                                $dbend_new = substr($dbend,0,5);
												if($dbday==$day66&&$dbmonth==$month66&&$dbyear==$year66 && $dbuser ==$user2)
												{
													echo '<div draggable="true" ondragstart="drag(event)" id="'.$dbid.'" class="title_day">' .$dbstart_new."-".$dbend_new." ".$dbtitle. '</div>';
												}
											}
        ?>
    </div>
    <!--Day 7-->    
    <div id ="<?php echo $year77."-".$month77."-".$day77;?>" class="box_date" style="left:1278px;" ondrop="drop(event)" ondragover="allowDrop(event)"><?php echo $day77."-".$month77."-".$year77;?>
        <?php
                                            $qry = "SELECT * FROM appointments ORDER BY time1";
											$result = $mysqli->query($qry);

											while ($row = $result->fetch_array())
											{
												$dbday = date('d',strtotime($row['date']));
												$dbmonth = date('m',strtotime($row['date']));
												$dbyear = date('Y',strtotime($row['date']));
												$dbtitle = $row['title'];
												$dbuser =$row['username'];
                                                $dbstart = $row['time1'];
                                                $dbend = $row['time2'];
                                                $dbid = $row['id'];
                                                $dbstart_new = substr($dbstart,0,5);
                                                $dbend_new = substr($dbend,0,5);
												if($dbday==$day77&&$dbmonth==$month77&&$dbyear==$year77 && $dbuser ==$user2)
												{
													echo '<div draggable="true" ondragstart="drag(event)" id="'.$dbid.'" class="title_day">' .$dbstart_new."-".$dbend_new." ".$dbtitle. '</div>';
												}
											}
        ?>
    </div> 
    </div>
  </body>
  <script>
	//Drag and Drop
	function allowDrop(ev) 
	{
		ev.preventDefault();
	}

	function drag(ev) 
	{
		ev.dataTransfer.setData("text", ev.target.id);
	}

	function drop(ev) 
	{
		ev.preventDefault();
		var data = ev.dataTransfer.getData("text");
		ev.target.appendChild(document.getElementById(data));
		var str = ev.target.lastChild.id;
        var str2 = ev.target.id;
        
        $.post( "update.php", { dateweek: str2, iddateweek: str  } );
	
	}
    </script>
</html>