<!--Check Session-->
<?php 
	session_start(); 
	if (!isset($_SESSION['username'])) 
	{
		$_SESSION['msg'] = "You must log in first";
		header('location: login.php');
	}
	if (isset($_GET['logout'])) 
	{
		session_destroy();
		$mysqli->close();
		unset($_SESSION['username']);
		header("location: login.php");
	}
?>

<!--DB Connect-->
<?php include "connect.php"?>
<?php
    $id = $_GET['id'];
?>

<html>
<head>
    <title> Edit</title>
    <link rel="stylesheet" type="text/css" href="css/edit.css" >
</head>
<body>
	<!--Get information to edit-->
    <?php
	            $qry = "SELECT * FROM appointments WHERE id=$id";
				$result = $mysqli->query($qry);

				while ($row = $result->fetch_array())
				{
                    $dbdate = $row['date'];
                    $dbtitle = $row['title'];
					$dbdetial = $row['details'];
                    $time = date('H',strtotime($row['time1']));         
                     $time2 = date('H',strtotime($row['time2']));
												
                }?>
				<!--Edit form-->
                <div class="form-style-8">
                    <form style="color:green;"action="calendar.php?date=<?php echo $_GET['date'];?> " method="post">
                        <input type="hidden" name="idedit" value="<?php echo $id; ?>" >
            			New date: <input style ="width:500px;"type="date" name="dateedit" value="<?php echo $dbdate; ?>" ><br>
                        New title : <input  style ="width:500px;" type="text" name="titleedit" value="<?php echo $dbtitle; ?>" /><br>
                        New details : <input style ="width:500px;" type="text" name="detailsedit" value="<?php echo $dbdetial; ?>"/><br>
                        New start-time : <input  style ="width:500px;"  type="time" name="timeStartedit" value="<?php echo $time;?>:00"/><br>
                        New end-time : <input  style ="width:500px;" type="time" name="timeEndedit" value="<?php echo $time2; ?>:00"/><br>
            			<center><input type="submit" value="Edit" >  <button style="height:34.8px; width:69.9px; background-color:#ff6666"><a style=" text-decoration:none; text-align:center; color:white; font-size: 14px;" href="calendar.php?date=<?php echo $_GET['date'];?> "> Cancel </button></center>
        		    </form>
                 </div>
                                           
</body>
</html>