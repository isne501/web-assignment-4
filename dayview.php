<!--Check Session user-->
<?php 
	session_start(); 
	if (!isset($_SESSION['username'])) 
	{
		$_SESSION['msg'] = "You must log in first";
		header('location: login.php');
	}
  if (isset($_GET['logout'])) 
  {
		session_destroy();
		unset($_SESSION['username']);
		header("location: login.php");
	}
?>
<!--Connect DB-->
<?php include "connect.php"?>
<!--HTML-->
<html style="font-family: 'Karla', sans-serif;">
     <head>
        <link href="https://fonts.googleapis.com/css?family=Karla" rel="stylesheet">
        <link rel="stylesheet" type="text/css" href="css/stylemenu.css" >
        <link rel="stylesheet" type="text/css" href="css/dayview.css" >
        <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
    </head>
    <!--Calendar-->
    <body class="margin" style="background:#FFE3BE">
        <div class="bg ">
            <?php
                $user2=$_SESSION['username'];
                $day = date('d', strtotime($_GET['date']));      
                $month = date('m', strtotime($_GET['date']));      
                $year = date('Y', strtotime($_GET['date'])); 
                $nameM = date('F', mktime(0,0,0, $month,10));
                    //print day month year
                    echo '<div class="day" >' .$day.'</div>';
                    echo '<div class="month" >' .$month.'</div>';
                    echo '<div class="year" >' .$year.'</div>';

                $newdate  = strtotime($_GET['date']);
                $nextM = date('Y-m-d',strtotime('+1 day',$newdate));
                $prevM = date('Y-m-d',strtotime('-1 day',$newdate));

	            $qry = "SELECT * FROM appointments";
                $result = $mysqli->query($qry);
            
                while($row = $result->fetch_array())
                {
                    $time = $row['time1'];
                    $time2 = $row['time2'];
	          }
            ?>
            <!-- Show username-->
            <span style="left: 526px;top: -109px;font-size: 42px;text-align: center;border: 1px solid;background-color:#ccffcc;width: 234px;height: 55px;border: 1px dashed;position: absolute;"><?php echo  $user2; ?></span>
            <!-- Month-->
            <a style="text-decoration: none; " href="calendar.php?date=<?php echo $_GET['date']; ?>"><span class="button" style="position:absolute;right: 1px;top: -48px;padding: 1px 15px;background-image: linear-gradient(120deg, #d4fc79 0%, #96e6a1 100%);box-shadow: none; color:black ; text-align: center;" >Month</span></a>
            <!-- Week-->
            <a style="text-decoration: none; " href="weekview.php?date=<?php echo $_GET['date']; ?>"><span class="button" style="position:absolute;right: 105px;top: -48px;padding: 1px 15px;background-image: linear-gradient(120deg, #d4fc79 0%, #96e6a1 100%);box-shadow: none; color:black ; text-align: center;" >Week</span></a>
            <!-- Next day-->
            <a style="text-decoration: none; " href="?date=<?php echo $prevM; ?>"><div class="button" style="position:absolute;left: 26px;padding: 1px 20px;top: -27px;background-color:#FF99CC;box-shadow:none;text-decoration: none;color:white;border-radius:10%;font-size: 19px;" >-</div></a>
            <!-- Previous day-->
            <a style="text-decoration: none; " href="?date=<?php echo $nextM; ?>"><div class="button" style="position:absolute;left: 26px;padding: 1px 20px;top: -139px;background-color:#FF99CC;box-shadow:none;text-decoration: none;color:white;border-radius:10%;font-size: 19px;">+</div></a>
            
            <!--print time-->
            <?php
                $array1= array();
                array_push($array1,'00','01','02','03','04','05','06','07','08','09','10','11','12','13','14','15','16','17','18','19','20','21','22','23');
                $user2 =$_SESSION['username'];
                $x=count($array1);

                for($i=0;  $i < $x ;$i++)
                {
                    echo '<div class="date" id="'. $array1[$i] . '" ondrop="drop1(event)" ondragover="allowDrop1(event)"><span  class="hr">' . $array1[$i]. '</span><br>';
                    $qry = "SELECT * FROM appointments";
                    $result = $mysqli->query($qry);
                    while ($row = $result->fetch_array())
                    {
                        $time = date('H',strtotime($row['time1']));
                        $time2 = date('H',strtotime($row['time2']));
                        substr($time,2);
                        substr($time2,2);
                        $title = $row['title'];
                        $dbday = date('d',strtotime($row['date']));
	                        $dbmonth = date('m',strtotime($row['date']));
	                        $dbyear = date('Y',strtotime($row['date']));
                            $dbuser = $row['username'];
                            $dbid = $row['id'];
                            $dbdetails = $row['details'];
                        if($dbday==$day && $dbmonth==$month && $dbyear==$year &&$dbuser==$user2)
                        {
                            if($array1[$i]==$time) //Show start time
                            {
                                echo  '<div class="title" id="'.'start'.$dbid.'" draggable="true" ondragstart="drag1(event)">'.'Start "' .$title."\"".'<span style="float:right;">' ."(Detail : ".$dbdetails.")".'</span>'.'</div>' ;
                            }
                            if($array1[$i]==$time2) //Show end time
                            {
                                echo  '<div class="stoptitle" id="'.'end'.$dbid.'" draggable="true" ondragstart="drag1(event)">' .'Stop. ' .$title .'</div>';
                            }   
                        }
                        }
                        echo '</div>';
                    }  
                    ?>
            </div>

    <script>
        //Drag and drop day view
        function allowDrop1(ev) 
        {
            ev.preventDefault();
        }

        function drag1(ev) 
        {
            ev.dataTransfer.setData("text", ev.target.id);
        }

        function drop1(ev) 
        {
            ev.preventDefault();
            var data = ev.dataTransfer.getData("text");
	        ev.target.appendChild(document.getElementById(data));
            var str = ev.target.lastChild.id;
            var str2= str.substr(0,1);
            var str3= str.substr(0,5);
            var str4 =str.substr(0,3);

            if(str2=="s")
            {
                if(str3=="start")
                {
                    var str5 = str.substr(5);
                    $.post( "update.php", { start: ev.target.id, idstart: str5  } );
                } 
            }
            else if(str2=="e")
            {
                if(str4=="end")
                {
                    var str6 = str.substr(3);
                    $.post( "update.php", { end: ev.target.id, idend: str6  } );
                } 
            }
        }
    </script>
  </body>
</html>