<?php include('server.php') ?>
<!DOCTYPE html>
<html>
<head>
	<title>Registration</title>
	<link rel="stylesheet" type="text/css" href="css/stylelogin.css">
</head>
<body>
	<div class="header">
		<h2>Sign Up!</h2>
	</div>
	<!--Register form-->
	<form method="post" action="register.php">

		<?php include('errors.php'); ?>

		<div class="input-group">
			<div class="input-group">
			<label>Email</label>
			<input type="email" name="email" value="<?php echo $email; ?>">
		</div>
			<label>Username</label>
			<input type="text" name="username" value="<?php echo $username; ?>">
		</div>
		
		<div class="input-group">
			<label>Password</label>
			<input type="password" name="password_1">
		</div>
		<div class="input-group">
			<label>Re-password</label>
			<input type="password" name="password_2">
		</div>
		<div class="input-group">
			<button type="submit" class="btn" name="reg_user">Register</button>
		</div>
		<p>
			Already has account? <a href="login.php">Sign in</a>
		</p>
	</form>
</body>
</html>