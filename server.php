<!--Insert User and check validation-->
<?php 
	session_start();
	$username = "";
	$email    = "";
	$errors = array(); 
	$_SESSION['success'] = "";
	$db = mysqli_connect('localhost', 'root', '', 'calendar');

	if (isset($_POST['reg_user'])) 
	{
		$username = mysqli_real_escape_string($db, $_POST['username']);
		$email = mysqli_real_escape_string($db, $_POST['email']);
		$password_1 = mysqli_real_escape_string($db, $_POST['password_1']);
		$password_2 = mysqli_real_escape_string($db, $_POST['password_2']);

		if (empty($username))
		{ 
			array_push($errors, "Username is required"); 
		}
		else if(strlen($username) <3)
		{
			array_push($errors, "Username at least 3 alphabets"); 
		}
		else if($username==trim($username) && strpos($username,' ')==true)
		{
			array_push($errors, "Username mustn't have space"); 
		}
		

		if (empty($email)) 
		{ 
			array_push($errors, "Email is required"); 
		}
		else if(!filter_var($email, FILTER_VALIDATE_EMAIL))
		{
			array_push($errors, "Invalid E-mail"); 
		}

		if (empty($password_1)) 
		{ 
			array_push($errors, "Password is required"); 
		}
		else if(strlen($password_1) < 8)
		{
			array_push($errors, "Password too short!"); 
		}
		else if(!preg_match("#[0-9]+#", $password_1))
		{
			array_push($errors, "Password must include at least one number!"); 
		}
		else if(!preg_match("#[a-z]+#", $password_1))
		{
			array_push($errors, "Password must include at least one lower-case character!"); 
		}
		else if(!preg_match("#[A-Z]+#", $password_1))
		{
			array_push($errors, "Password must include at least one upper-case character!"); 
		}
		else if (ctype_alnum($password_1))
		{
			array_push($errors, "Password must include at least one special character!");
		}

		if ($password_1 != $password_2) 
		{
			array_push($errors, "The two passwords do not match");
		}
		
		$query = "SELECT * FROM users";
		$result = mysqli_query($db, $query);
		while($row = mysqli_fetch_array($result, MYSQLI_BOTH))
		{
			if($row['username']==$username)
			{
				array_push($errors, "Try another username");
				break;
			}
		}
		

		if (count($errors) == 0) 
		{
			$password = md5($password_1);
			$query = "INSERT INTO users (username, email, password) 
					  VALUES('$username', '$email', '$password')";
			mysqli_query($db, $query);

			$_SESSION['username'] = $username;
			$_SESSION['success'] = "You are now logged in";
			header('location: index.php');
		}
	}

	if (isset($_POST['login_user'])) 
	{
		$username = mysqli_real_escape_string($db, $_POST['username']);
		$password = mysqli_real_escape_string($db, $_POST['password']);

		if (empty($username)) 
		{
			array_push($errors, "Username is required");
		}
		if (empty($password)) 
		{
			array_push($errors, "Password is required");
		}

		if (count($errors) == 0) 
		{
			$password = md5($password);
			$query = "SELECT * FROM users WHERE username='$username' AND password='$password'";
			$results = mysqli_query($db, $query);

			if (mysqli_num_rows($results) == 1) 
			{
				$_SESSION['username'] = $username;
				$_SESSION['success'] = "You are now logged in";
				header('location: index.php');
			}
			else
			{
				array_push($errors, "Wrong username or password");
			}
		}
	}
?>